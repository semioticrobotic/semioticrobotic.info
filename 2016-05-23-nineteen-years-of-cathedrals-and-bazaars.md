Permalink: nineteen-years-of-cathedrals-and-bazaars

# Nineteen years of cathedrals and bazaars

Today Opensource.com [published my reflection](https://opensource.com/life/16/5/19-years-later-cathedral-and-bazaar-still-moves-us) on *The Cathedral and the Bazaar*, which I wrote in honor of the document's 19th anniversary:

> Whether Raymond's insights hold true—whether they "accurately" describe the contemporary political, social, and economic conditions for open source *anything*—are inconsequential in light of the way *The Cathedral and the Bazaar* actually *functions* today. Quite simply, what the book says is not nearly as important as what it *does*.
