Permalink: giving-thanks

# Giving thanks

Since I couldn't be in Pennsylvania for Thanksgiving this year, Erin asked if I'd like to spend the day with her family and extended family—you know, some real, honest-to-gosh, down home Maine-ahs. Erin's mom wanted me to sign a waiver absolving the family of any responsibility for emotional or psychological damages I incurred as a result of the visit, but I'm more than happy to say I fared just fine and had a lovely time.

I got my first glimpse into Erin's upbringing before we even hit the front stairs of her aunt's house. She was showing me her cousin's prized pumpkins and sunflowers when I saw something lumbering in the shrubs.

"Erin, I think I see something moving in the—"

"What?! What is it? Where??"

She burst into the house, and by the time I made it up the steps eleven family members had their noses pressed against the front window.

"Is it a ground hog?"

"I think it's a porcupine."

"Can I shoot it?"

"What? A porcupine?"

"Get your gun!"

"Wow, that's really close."

"Can I shoot it?"

"It could hurt the dogs."

"Dad, can I shoot it?"

"What kind of gun would you use?"

And then this part of the conversation spiraled into a discussion of firearms that involved many numbers and letters, which were over my head. (The porcupine is safe, by the way. **Update: Reports confirm the porcupine has now been assassinated.**)

Dinner, as my currently-gurgling stomach can attest, was delicious and unlike anything I've ever seen. After utilizing the extent of my culinary skills to microwave peas, squash, potatoes and onions, I ate quietly while listening to Erin's cousin talk about showing "hausses"—which in PA Dutch might make her sound like she's in real estate. But Maine-ahs understand that she exhibits and rides equine competitively (and apparently quite successfully; the house is teeming with medals and trophies).

Around this point, one of Erin's relatives turned to me and jabbed a fork in my direction.

"On New Year's Day," she said, eyes slits, "what do you eat?"

"Pork and sauerkraut. If you don't eat it that day, it's bad luck for the rest of the year."

"I knew it!" Her pursed lips belied her antipathy for one of my favorite dishes in the world.

"You don't like it?"

Her face grew more grim. I had my answer.

Erin's aunt—who affectionately refers to her sibling as "sistah"—was quite upset that the turkey she'd prepared weighed only 23.67 pounds (she stressed this number with more consternation each time she recited it). The butcher had apparently promised her a bird of 25-26 pounds, and as one could see from the leftovers, we certainly missed those 1.33 extra pounds of food. She was, however, pleased with the pie-to-person ratio, which was a more comfortable 1:2.

This Thanksgiving, I was simply most thankful for an extremely warm and kind group of people who would take a stranger into their home, let him buzz around them with a digital camera, listen to a very boring explanation of his Masters thesis research, follow along patiently as he outlined the proper way to order a cheese steak in Philadelphia, and stuff him so full of food he couldn't remember his own name.
