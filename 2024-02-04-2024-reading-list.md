Permalink: 2024-reading-list

# My 2024 reading list

An ongoing list of books I've read in 2024.

* [Random Thoughts](https://scottnesbitt.gumroad.com/l/randomthoughts) by Scott Nesbitt
* [In the Beginning ... Was the Command Line](https://bookshop.org/a/99558/9780380815937) by Neal Stephenson
* [Tomorrow, and Tomorrow, and Tomorrow](https://bookshop.org/a/99558/9780593321201) by Gabrielle Zevin
* [X-Men: Generation X (Volume 1)](https://search.worldcat.org/title/666234792) by Scott Lobdell
* [Quietly Hostile](https://bookshop.org/a/99558/9780593315699) by Samantha Irby
* [Me (Moth)](https://bookshop.org/a/99558/9781250833037) by Amber McBride
* [The Three-Body Problem](https://bookshop.org/a/99558/9780765382030) by Cixin Liu
* [Artificial Condition](https://bookshop.org/a/99558/9781250186928) by Martha Wells
* [The Little Book of Common Sense Investing](https://bookshop.org/a/99558/9781119404507) by John C. Bogle
* [Pink Slime](https://bookshop.org/a/99558/9781668049778) by Fernanda Trías
* [Novelist as a Vocation](https://bookshop.org/a/99558/9781101974537) by Haruki Murakami
* [In Our Likeness](https://bookshop.org/a/99558/9781662522611) by Bryan Vandyke
* [Hum](https://bookshop.org/a/99558/9781668008836) by Helen Phillips

See also: [The collection on Bookshop.org](https://bookshop.org/lists/2024-reading-list-semioticrobotic).
