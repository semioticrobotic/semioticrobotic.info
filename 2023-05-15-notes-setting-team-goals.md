Permalink: notes-setting-team-goals

# Notes on setting team goals

Setting collectively held, team-focused goals can be an incredibly useful technique for focusing collaborative effort and meeting organizational objectives in a specified period of time.

The following are my notes on a goal-setting technique I've advocated with teams in various organizations. It's a modification and specification of the popular Objectives and Key Results ([OKR](https://en.wikipedia.org/wiki/Objectives_and_key_results)) tool Google has popularized. I call it "the O³ framework," as it involves three fundamental components:

* Objectives
* Outcomes
* Outputs

Let's examine each of these components, from most abstract to most concrete.

## Objectives

Objectives express the grand designs you and your team hope to achieve—the world you hope to bring about, or the state you hope to espouse. Articulating objectives involves casting forward and answering "big questions" like *What is the point of our collective work?* and *Why are we doing what we're doing, anyway?* Objectives are aspirational, grand; they speak to your team's *raison d'être*. They might be things like:

* Make our product the most popular product in its class
* Help our company become the place to work
* Turn our platform into the default platform for customers

... something like that. Objectives are what you want your hard work to accomplish, the difference it's all going to make.

## Outcomes

Outcomes express what you and your team determine all your work is going produce in the service of your objectives. They describe the concrete effects of your activities. Your team articulates outcomes by answering the question *How will we know that we have achieved our objective?* Outcomes might be things like:

* Increase press mentions of our product by 30 percent this fiscal quarter
* Achieve at least 90 percent satisfaction rating on Glassdoor by year's end
* Increase developer use of the platform by 15% by mid-year

... something like that. Outcomes are *discrete* (they don't overlap). They are *measureable* (you can easily determine whether you're achieving them). And they are *time-bound* (they have a clear timeline, an end-date or goal-state). Perhaps most importantly: Outcomes are not *what you and your team are going to do* in any given tim period; they are the *results* of what you are going to do. They are what you are doing *is going to do*.

That is to say, they are the *outcomes* of your work.

## Outputs

Outputs describe nitty-gritty, daily tactics—the everyday "stuff of work." These are all the *things you are going to do* to produce your outcomes and achieve your objectives. They might be things like:

* Send at least 20 PR releases to media sources by end of quarter
* Redesign new-hire feedback process to include Glassdoor incentive
* Launch a social media campaign that includes at least 15 new assets for LinkedIn

...whatever you think you need to do to achieve your outcomes. While objectives and outcomes should be *collective* (everyone on the team, managers included, unite to develop and commit to them) outputs can be *individual* (they identify ways that every team member can ply their unique talents to create things that advance the team's overall goals).

Outputs are the activities that constitute someone's "job responsibilities," and every team member should be developing these in ongoing conversation with their respective managers or team leads. This way, team members and managers alike are aware of what everyone is doing on a daily basis to help the team achieve its collective outcomes. Reflecting on one's current slate of responsibilities, one might ask: *What am I "outputting" to bring about the state I've agreed I wish to bring about?*

No matter their role, all team members should make their outputs *transparent*. Everyone on the team should be able to see what everyone else is responsible for *doing* to help the team achieve its objective(s). This increases accountability (we all know who's responsible for what) and collaboration (if we see opportunities to help one another achieve these things, we can seize them).

## Putting it together

So you have **outputs** (what you are doing), **outcomes** (what you want that stuff you're doing *to do*), and **objectives** (what you want to happen because the stuff you've done has had the effect that it's had).

So: O³.

Identifying and clearly specifying every element allows team members to better understand the most meaningful way(s) their everyday work aids the collective (the team or the organization). Organizing earlier examples, we can see how the increasingly concrete nature of each O³ element makes this possible.

| Objective | Outcome | Output |
| --------- | ------- | ------ |
| Make our product the most popular product in its class | Increase press mentions of our product by 30 percent this fiscal quarter | Send at least 20 PR releases to media sources by end of quarter |
| Help our company become the place to work | Achieve at least 90 percent satisfaction rating on Glassdoor by year's end | Redesign new-hire feedback process to include Glassdoor incentive |
| Turn our platform into the default platform for customers | Increase developer use of the platform by 15% by mid-year | Launch a social media campaign that includes at least 15 new assets for LinkedIn |

Ultimately, the O³ framework is useful because it fosters ongoing conversation and effective decision-making, especially when teams find themselves weighing priorities and assessing competing demands on team members' time. Everyone on a team can appeal to a collectively defined O³ matrix to determine whether and how new work or incoming requests aid the team's overall vision—and make smarter decisions about how to handle that work (if at all).

Helping a team maintain collective perspective is actually one of the greatest benefits of O³. Using the model helps teams make difficult decisions about the way everyone spends their time and prioritize work that is most impactful.

For instance: Outputs are the most concrete element of O³ and therefore the most easily "graspable" when folks sitt down to set priorities for a year or a quarter. But teams with an exclusive or myopic focus on *outputs* risk losing sight of the *point* of their everyday work (the desired *outcomes*). The point of work is not to "do X things every week"; the point is to "achieve X things every week." So for teams seeking clarity, the question is never only *Did we do X things this week?* It's also *"Did doing X things this week help us achieve Y goal this week?* If so, great! Maybe you wish to keep doing those things. And if not, then ask: Does achieving that outcome require different outputs from our team?

Put another way: Is what you're doing—or what your team is being asked to do—something that will help the entire team achieve its intended outcomes? If not, then it's time to have a conversation about switching daily tactics to produce new kinds of outputs that might get everyone there. And everyone should always be tracking outcomes—weekly, ideally, or quarterly, at a minimum, to ensure that the things they're outputting are actually achieving those outcomes. If not, everyone can have a conversation about why those tactics didn't work, and they can change tack much easier.

## Resources

The following books can assist with goal-setting in general (and O³ in particular):

- [Radical Focus](https://cwodtke.com/writing-2/) by Christina Wodtke
- [Measure what Matters](https://www.whatmatters.com/the-book) by John Doer

