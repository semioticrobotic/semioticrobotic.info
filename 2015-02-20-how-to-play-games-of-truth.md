Permalink: how-to-play-games-of-truth

# How to play games of truth

My article, "How to Play Games of Truth," recently appeared in a [special issue](http://www.syllabusjournal.org/syllabus/issue/view/9) of [*Syllabus*](http://www.syllabusjournal.org/) devoted to "teaching with and about video games." Editors Jennifer deWinter and Carly A. Kocurek have assembled an outstanding resource for anyone interested not only in *teaching in* game studies but also in *using* games to create unique educational experiences across the curriculum. In keeping with the journal's open access policy, [my article](http://www.syllabusjournal.org/syllabus/article/view/104) is available for [download](http://www.syllabusjournal.org/syllabus/article/view/104/pdf_2) without restriction.
