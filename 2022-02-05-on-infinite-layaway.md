Permalink: on-infinite-layaway

# On infinite layaway

At the start of the year I decided I'd collect back issues of my [vaporwave-themed newsletter](http://semioticrobotic.email) into a single, book-length volume. It's called *Infinite Layaway*, and it's [out now](http://semioticrobotic.shop).

I really had fun making it—brainstorming and debating potential titles with my partner, remixing the issues to create a novel sense of flow for readers, and plying my poor graphic design skills in pursuit of a serviceable cover. I also wrote a new introduction, which I'll record below.

-----

Time worked differently in 2020. As the COVID-19 pandemic dislocated everyone and everything, seconds became days became hours became minutes became months became meaningless.

That constant temporal condensation and expansion had a funny effect on everyone's sense of continuity, of succession. The period was vaporous. I felt time as an overabundance, my everyday supersaturated, all the motions there, but without many of the usual anchors. Unmoored and adrift, I decided I needed to stabilize myself with something entirely outside my typical coordinates—something typically infeasible amid the interstices of work and school, but that this odd temporal cracking now made viable.

So I decided to write about vaporwave.

Vaporwave appeals to me most during moments like these, when time isn't operating in familiar fashion. Occasionally I might even use vaporwave for my own selfish temporal manipulations—say, when I want to prolong that perfect morning just a little longer, or prefer to experience nighttime in the daytime. I'm not surprised I found it especially appealing in the midst of 2020's bizarre, chronic pulsations. For the first time, however, I felt both the opportunity and the permission to pay vaporwave the kind of attention I've always wanted to. Maybe I could finally write my way to figuring out why I love this thing so much.

And yet while I felt time as surfeit, I also registered a distinct deficit—of interpersonal connection, of the rituals and routines that bind a person to others and likewise give any period its shape and texture. Like most others stuck inside for all but the occasional daily walk, I sought some way to make this seemingly solitary activity a social one.

I imagined the most straightforward effort I could: Write about one vaporwave song every week, for as much or as little as I wanted to, then package that writing in a newsletter and beam it to the email inboxes of anyone who'd like to listen to that song too (and maybe read what it made me think and feel while they did so). The fact that I gave no real consideration to matters like tone and format should be obvious from the work. Some pieces read like vignettes, snapshots of entirely fictitious scenarios that whatever song I've heard seem to conjure for me. Others are more like essays, probing more deeply into vaporwave's mechanics, effects, stakes, and claims. It doesn't make much sense, but ultimately I found that it was the best way I could satisfactorily explore vaporwave: vacillating back and forth between the imagined and the expository, the fantastical and the exegetical. Taken together, I hope all these tidbits—these little plastic daydreams—add up to something that advances the ongoing conversation about vaporwave—as a genre, a movement, an impulse. Really, though, I'm just grateful people subscribed, read, and wrote me back.

This book collects, reshuffles, and repackages issues of the newsletter as a kind of stock-taking exercise—an account of what I've heard, felt, and learned so far. Where I noted misspellings or other errors, I've corrected them, but apart from that (and this introduction), I've made no significant changes to the writing. Every newsletter issue begins with a hyperlink to the song that inspires it; in the book-length compilation, I've reproduced those links as footnotes, in case the reader wishes to spend some time with the music that animated the writing.

I'll add to this book when I'm able, whenever I release new series of letters. I doubt it will ever culminate, congeal. A vaporbook shouldn't. Perpetually deferring coherent conclusions is vaporwave's *modus operandi*. I'll just keep writing in search of it here, offering it up every now and again, inviting readers to invest in it forever, and keeping anything finished or final on infinite layaway.
